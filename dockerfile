FROM openjdk:11.0.7-slim-buster
MAINTAINER Nanoo (arnaudlaval33@gmail.com)
ADD target/tutorial-management.jar tutorial-management.jar
ENTRYPOINT ["java","-jar","tutorial-management.jar"]