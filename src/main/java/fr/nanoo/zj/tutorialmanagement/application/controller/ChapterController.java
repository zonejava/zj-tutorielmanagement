package fr.nanoo.zj.tutorialmanagement.application.controller;

import fr.nanoo.zj.tutorialmanagement.application.errors.FunctionalException;
import fr.nanoo.zj.tutorialmanagement.model.dtos.ChapterDto;
import fr.nanoo.zj.tutorialmanagement.model.dtos.VideoDto;
import fr.nanoo.zj.tutorialmanagement.service.contract.ChapterService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

/**
 * @author nanoo - created : 12/06/2020 - 19:06
 */
@PreAuthorize("isAuthenticated()")
@RestController
@Slf4j
public class ChapterController {

    private final ChapterService chapterService;

    @Autowired
    public ChapterController(ChapterService chapterService) {
        this.chapterService = chapterService;
    }

    @PutMapping("/chapter/add/{id}")
    public ChapterDto addChapterToTutorial (@RequestBody ChapterDto chapterDto, @PathVariable("id") String tutorialExternalId){
        log.debug("add chapter method entry for tutorial : " + tutorialExternalId);
        try {
            return chapterService.addChapterToTutorial(chapterDto,tutorialExternalId);
        }catch (FunctionalException e){
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST, "Error during chapter insertion in database", e
            );
        }
    }

    @PutMapping("/chapter/update")
    public ChapterDto updateChapter(@RequestBody ChapterDto chapterDto){
        log.debug("update chapter method entry");
        try {
            return chapterService.updateChapter(chapterDto);
        }catch (FunctionalException e){
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST, "Error during chapter update in database", e
            );
        }
    }

    @DeleteMapping("/chapter/remove/{id}")
    public void removeChapterFromTutorial (@PathVariable("id") String chapterExternalId){
        log.debug("remove chapter method entry with extId : " + chapterExternalId);
        try {
            chapterService.removeChapterFromTutorial(chapterExternalId);
        }catch (FunctionalException e){
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST, "Error during chapter deletion from database", e
            );
        }
    }

    @PutMapping("/chapter/video/add/{id}")
    public void addVideoToChapter (@PathVariable("id") String chapterExternalId, @RequestBody VideoDto videoDto){
        log.debug("add video in chapter method entry with extId : " + chapterExternalId);
        try {
            chapterService.addVideoToChapter(videoDto,chapterExternalId);
        }catch (FunctionalException e){
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST, "Error during video insertion in chapter", e
            );
        }
    }

    @DeleteMapping("/chapter/video/remove/{id}")
    public void removeVideoFromChapter (@PathVariable("id") String videoExterenalId){
        log.debug("remove video from chapter method entry with extId : " + videoExterenalId);
        try {
            chapterService.removeVideoFromChapter(videoExterenalId);
        }catch (FunctionalException e){
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST, "Error during video deletion from chapter", e
            );
        }
    }

}
