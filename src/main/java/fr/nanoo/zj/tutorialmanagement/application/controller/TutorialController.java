package fr.nanoo.zj.tutorialmanagement.application.controller;

import fr.nanoo.zj.tutorialmanagement.application.errors.FunctionalException;
import fr.nanoo.zj.tutorialmanagement.model.dtos.RepositoryDto;
import fr.nanoo.zj.tutorialmanagement.model.dtos.TutorialDto;
import fr.nanoo.zj.tutorialmanagement.model.enumerations.Status;
import fr.nanoo.zj.tutorialmanagement.service.contract.TutorialService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

/**
 * @author nanoo - created : 11/06/2020 - 17:15
 */
@PreAuthorize("isAuthenticated()")
@RestController
@Slf4j
public class TutorialController {

    private final TutorialService tutorialService;

    @Autowired
    public TutorialController(TutorialService tutorialService) {
        this.tutorialService = tutorialService;
    }

    @PutMapping("/tutorial/save")
    public TutorialDto createTutorial(@RequestBody TutorialDto tutorialDto){
        log.debug("save tutorial method entry");
        try {
            return tutorialService.saveTutorial(tutorialDto);
        }catch (Exception e){
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST, "Error during tutorial insertion in database", e
            );
        }
    }

    @PutMapping("/tutorial/update")
    public TutorialDto updateTutorial(@RequestBody TutorialDto tutorialDto){
        log.debug("update tutorial method entry");
        try {
            return tutorialService.updateTutorial(tutorialDto);
        }catch (FunctionalException e){
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST, "Error during tutorial update in database", e
            );
        }
    }

    @DeleteMapping("/tutorial/delete/{id}")
    public void deleteTutorial(@PathVariable("id") String externalId){
        log.debug("delete tutorial method entry");
        try {
            tutorialService.deleteTutorial(externalId);
        }catch (FunctionalException e){
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST, "Error during tutorial deletion from database", e
            );
        }
    }

    @PreAuthorize("permitAll()")
    @GetMapping("/tutorial/{id}")
    public TutorialDto getTutorial (@PathVariable("id") String externalId){
        log.debug("get tutorial method entry for id : " + externalId);
        try {
            return tutorialService.getTutorial(externalId);
        }catch (Exception e){
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST, "Error during tutorial search in database", e
            );
        }
    }

    @PreAuthorize("permitAll()")
    @GetMapping("/tutorials/published/{page}")
    public Page<TutorialDto> getAllPublishedTutorials(@RequestParam(value = "search", defaultValue = "all") String keyword, @PathVariable("page") int page){
        log.debug("get published tutorials search method entry with keyword : " + keyword);
        try {
            return tutorialService.getTutorials(Status.PUBLISHED, keyword, page);
        }catch (Exception e){
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST, "Error during published tutorials search in database", e
            );
        }
    }

    @PreAuthorize("permitAll()")
    @GetMapping("/user/tutorial/{id}")
    public List<TutorialDto> getWriterAllTutorials (@PathVariable("id") String externalId){
        log.debug("get writer's tutorials method entry for id : " + externalId);
        try {
            return tutorialService.getTutorialsByWriterExternalId(externalId);
        }catch (Exception e){
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST, "Error during writer's tutorials search in database", e
            );
        }
    }

    @PutMapping("/tutorial/repo/add/{id}")
    public RepositoryDto addRepositoryToTutorial(@PathVariable("id") String tutorialExternalIdId, @RequestBody RepositoryDto repositoryDto){
        log.debug("add repository to tutorial method entry");
        try {
            return tutorialService.addRepositoryToTutorial(tutorialExternalIdId,repositoryDto);
        }catch (FunctionalException e){
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST, "Error during repository add to tutorial", e
            );
        }
    }

    @DeleteMapping("/tutorial/repo/remove/{id}")
    public void removeRepositoryFromTutorial (@PathVariable("id") String tutorialExternalId) {
        log.debug("remove repository from chapter method entry");
        try {
            tutorialService.removeRepositoryFromTutorial(tutorialExternalId);
        } catch (FunctionalException e) {
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST, "Error during video deletion from chapter", e
            );
        }
    }

    @GetMapping("/tutorial/publish/{id}")
    public TutorialDto setTutorialStatusPublished (@PathVariable("id") String externalId){
        log.debug("set tutorial status publish method entry for id : " + externalId);
        try {
            return tutorialService.updateTutorialStatus(externalId, Status.PUBLISHED);
        }catch (Exception e){
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST, "Error during tutorial status update", e
            );
        }
    }

    @GetMapping("/tutorial/unpublish/{id}")
    public TutorialDto setTutorialStatusUnpublished (@PathVariable("id") String externalId){
        log.debug("set tutorial status un unpublish method entry for id : " + externalId);
        try {
            return tutorialService.updateTutorialStatus(externalId, Status.NOT_PUPLISHED);
        }catch (FunctionalException e){
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST, "Error during tutorial status update", e
            );
        }
    }

}
