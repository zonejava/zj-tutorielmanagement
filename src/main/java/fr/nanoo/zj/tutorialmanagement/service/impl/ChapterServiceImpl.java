package fr.nanoo.zj.tutorialmanagement.service.impl;

import fr.nanoo.zj.tutorialmanagement.application.errors.FunctionalException;
import fr.nanoo.zj.tutorialmanagement.db.ChapterRepository;
import fr.nanoo.zj.tutorialmanagement.db.TutorialRepository;
import fr.nanoo.zj.tutorialmanagement.db.VideoRepository;
import fr.nanoo.zj.tutorialmanagement.model.dtos.ChapterDto;
import fr.nanoo.zj.tutorialmanagement.model.dtos.VideoDto;
import fr.nanoo.zj.tutorialmanagement.model.entities.Chapter;
import fr.nanoo.zj.tutorialmanagement.model.entities.Tutorial;
import fr.nanoo.zj.tutorialmanagement.model.mappers.ChapterMapper;
import fr.nanoo.zj.tutorialmanagement.model.mappers.VideoMapper;
import fr.nanoo.zj.tutorialmanagement.security.SecurityUtils;
import fr.nanoo.zj.tutorialmanagement.service.contract.ChapterService;
import fr.nanoo.zj.tutorialmanagement.utils.IdGenerator;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.HibernateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @author nanoo - created : 12/06/2020 - 19:11
 */
@Service
@Transactional
@Slf4j
public class ChapterServiceImpl implements ChapterService {

    private final TutorialRepository tutorialRepository;
    private final ChapterRepository chapterRepository;
    private final VideoRepository videoRepository;
    private final ChapterMapper chapterMapper;
    private final VideoMapper videoMapper;

    @Autowired
    public ChapterServiceImpl(TutorialRepository tutorialRepository, ChapterRepository chapterRepository,
                              VideoRepository videoRepository, ChapterMapper chapterMapper, VideoMapper videoMapper) {
        this.tutorialRepository = tutorialRepository;
        this.chapterRepository = chapterRepository;
        this.videoRepository = videoRepository;
        this.chapterMapper = chapterMapper;
        this.videoMapper = videoMapper;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ChapterDto addChapterToTutorial(ChapterDto chapterDto, String tutorialExternalId) {
        Tutorial tutorial = tutorialRepository.findByExternalId(tutorialExternalId).orElseThrow();
        List<Chapter> chapters = tutorial.getChapters();

        SecurityUtils.checkIfAuthorized(tutorial.getWriter().getExternalId());

        Chapter chapterToSave = chapterMapper.fromDtoToEntity(chapterDto);
        chapterToSave.setExternalId(IdGenerator.generateExternalId());
        chapterToSave.setTutorial(tutorial);
        chapterToSave.setCreationChapterDate(LocalDateTime.now());
        chapterToSave.setTitle("Chapitre n°" + (chapters.size() + 1));
        chapterToSave.setContent("vide");

        return chapterMapper.fromEntityToDto(chapterRepository.save(chapterToSave));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ChapterDto updateChapter(ChapterDto chapterDto) {
        Chapter chapterToSave = chapterMapper.fromDtoToEntity(chapterDto);

        Chapter oldVersion = chapterRepository.findByExternalId(chapterToSave.getExternalId()).orElse(null);
        if (oldVersion == null)
            throw new FunctionalException("Error in chapter update processus, old version not found in DB");

        SecurityUtils.checkIfAuthorized(oldVersion.getTutorial().getWriter().getExternalId());

        chapterToSave.setUpdateChapterDate(LocalDateTime.now());
        chapterToSave.setChapterId(oldVersion.getChapterId());
        chapterToSave.setTutorial(oldVersion.getTutorial());
        chapterToSave.setVideo(oldVersion.getVideo());

        return chapterMapper.fromEntityToDto(chapterRepository.save(chapterToSave));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void removeChapterFromTutorial(String chapterExternalId){
        try{
            Chapter chapterToDelete = chapterRepository.findByExternalId(chapterExternalId).orElse(null);
            if (chapterToDelete == null)
                throw new FunctionalException("Error, chapter not found in DB");

            SecurityUtils.checkIfAuthorized(chapterToDelete.getTutorial().getWriter().getExternalId());

            chapterRepository.delete(chapterToDelete);
        }catch (HibernateException e){
            log.error("error during chapter deletion with extId : " + chapterExternalId);
            throw new FunctionalException("error during chapter deletion : " + e.getMessage());
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void addVideoToChapter(VideoDto videoDto, String chapterExternalId){
        try{
            Chapter chapter = chapterRepository.findByExternalId(chapterExternalId).orElse(null);
            if (chapter == null){
                throw new FunctionalException("Error during insertion video in chapter, chapter not found!");
            }

            SecurityUtils.checkIfAuthorized(chapter.getTutorial().getWriter().getExternalId());

            videoDto.setExternalId(IdGenerator.generateExternalId());
            videoDto.setAddVideoDate(LocalDateTime.now());
            chapter.setVideo(videoRepository.save(videoMapper.fromDtoToEntity(videoDto)));
            chapterRepository.save(chapter);

        }catch (HibernateException e){
            log.error("error during video insertion in chapter with extId : " + chapterExternalId);
            throw new FunctionalException("error during video insertion : " + e.getMessage());
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void removeVideoFromChapter(String videoExterenalId) {
        try{
            Chapter chapter = chapterRepository.findChapterWithVideoToRemove(videoExterenalId);

            SecurityUtils.checkIfAuthorized(chapter.getTutorial().getWriter().getExternalId());

            chapter.setVideo(null);
        }catch (HibernateException e){
            log.error("error during video deletion from chapter");
            throw new FunctionalException("error during video deletion : " + e.getMessage());
        }
    }
}
