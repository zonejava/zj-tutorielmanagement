package fr.nanoo.zj.tutorialmanagement.service.contract;

import fr.nanoo.zj.tutorialmanagement.model.dtos.RepositoryDto;
import fr.nanoo.zj.tutorialmanagement.model.dtos.TutorialDto;
import fr.nanoo.zj.tutorialmanagement.model.enumerations.Status;
import org.springframework.data.domain.Page;

import java.util.List;

/**
 * @author nanoo - created : 24/06/2020 - 17:46
 */
public interface TutorialService {

    /**
     * This method save a tutorial in DB.
     *
     * @param tutorialDto tutorial to save
     * @return tutorial if success
     */
    TutorialDto saveTutorial(TutorialDto tutorialDto);

    /**
     * This methode do change in tutorial for update process
     *
     * @param tutorialDto tutorial to update
     */
    TutorialDto updateTutorial(TutorialDto tutorialDto);

    /**
     * This method get a tutorial in DB.
     *
     * @param externalId id of tutorial
     * @return tutorial if present in DB
     */
    TutorialDto getTutorial(String externalId);


    /**
     * This method search all tutorials in DB wich status wanted
     *
     * @param status  status of tutorials
     * @param keyword keyword match search in title and description
     * @param page    number of the page requested
     * @return a list of tutorials if exist
     */
    Page<TutorialDto> getTutorials(Status status, String keyword, int page);

    /**
     * This method search all tutorials in Db for a particular writer
     *
     * @param externalId externalId of writer
     * @return a list of tutorial if exist
     */
    List<TutorialDto> getTutorialsByWriterExternalId(String externalId);

    /**
     * This method add a repository to a tutorial
     *
     * @param tutorialExternalId tutorial we add repo to
     * @param repositoryDto      tutorial to add
     * @return repository if success
     */
    RepositoryDto addRepositoryToTutorial(String tutorialExternalId, RepositoryDto repositoryDto);

    /**
     * This method remove a repository from a tutorial and DB
     *
     * @param repositoryExternalId repository to remove
     */
    void removeRepositoryFromTutorial(String repositoryExternalId);

    /**
     * This method update a tutorial status
     *
     * @param externalId tutorial to update
     * @param status     new status
     * @return the tutorial if success
     */
    TutorialDto updateTutorialStatus(String externalId, Status status);

    /**
     * This method remove a tutorial from DB
     *
     * @param externalId tutorial to remove
     */
    void deleteTutorial(String externalId);

}
