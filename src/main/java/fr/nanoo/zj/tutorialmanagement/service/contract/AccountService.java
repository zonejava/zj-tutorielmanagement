package fr.nanoo.zj.tutorialmanagement.service.contract;

import fr.nanoo.zj.tutorialmanagement.model.dtos.AccountDto;
import fr.nanoo.zj.tutorialmanagement.model.entities.Account;

/**
 * @author nanoo - created : 10/06/2020 - 19:40
 */
public interface AccountService {

    /**
     * This method save a user in DataBase.
     * @param user user to save
     * @return user if save is success
     */
    AccountDto saveUser(AccountDto user);

    /**
     * This method remove a user from Database.
     * @param id user id to remove
     */
    void deleteUserAccount(String id);

    /**
     * This method get account information in DB.
     * @param externalId id of user
     * @return user
     */
    Account getUserAccount(String externalId);
}
