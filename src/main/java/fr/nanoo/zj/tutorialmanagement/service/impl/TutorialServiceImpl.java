package fr.nanoo.zj.tutorialmanagement.service.impl;

import fr.nanoo.zj.tutorialmanagement.application.errors.FunctionalException;
import fr.nanoo.zj.tutorialmanagement.db.RemoteRepositoryRepository;
import fr.nanoo.zj.tutorialmanagement.db.TutorialRepository;
import fr.nanoo.zj.tutorialmanagement.model.dtos.RepositoryDto;
import fr.nanoo.zj.tutorialmanagement.model.dtos.TutorialDto;
import fr.nanoo.zj.tutorialmanagement.model.entities.Account;
import fr.nanoo.zj.tutorialmanagement.model.entities.Tutorial;
import fr.nanoo.zj.tutorialmanagement.model.enumerations.Status;
import fr.nanoo.zj.tutorialmanagement.model.mappers.RepositoryMapper;
import fr.nanoo.zj.tutorialmanagement.model.mappers.TutorialMapper;
import fr.nanoo.zj.tutorialmanagement.security.SecurityUtils;
import fr.nanoo.zj.tutorialmanagement.service.contract.AccountService;
import fr.nanoo.zj.tutorialmanagement.service.contract.TutorialService;
import fr.nanoo.zj.tutorialmanagement.utils.IdGenerator;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.HibernateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @author nanoo - created : 11/06/2020 - 17:20
 */
@Service
@Transactional
@Slf4j
public class TutorialServiceImpl implements TutorialService {

    private final TutorialRepository tutorialRepository;
    private final TutorialMapper tutorialMapper;
    private final RemoteRepositoryRepository remoteRepository;
    private final RepositoryMapper repositoryMapper;

    private final AccountService accountService;

    @Autowired
    public TutorialServiceImpl(TutorialRepository tutorialRepository, TutorialMapper tutorialMapper, RemoteRepositoryRepository remoteRepository, RepositoryMapper repositoryMapper, AccountService accountService) {
        this.tutorialRepository = tutorialRepository;
        this.tutorialMapper = tutorialMapper;
        this.remoteRepository = remoteRepository;
        this.repositoryMapper = repositoryMapper;
        this.accountService = accountService;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public TutorialDto saveTutorial(TutorialDto tutorialDto) {
        setTitleLayout(tutorialDto);
        Tutorial tutorialToSave = tutorialMapper.fromDtoToEntity(tutorialDto);

        /* First retrieve writer info */
        String writerId = AccountServiceImpl.getUserLoggedExternalId();
        log.debug("*** User external id retrieve in tutorial create method : " + writerId);
        Account writer = accountService.getUserAccount(writerId);
        if (writer == null) throw new FunctionalException("No writer account found in DB");

        /* Second fill some field in tutorial */
        tutorialToSave.setStatus(Status.NOT_PUPLISHED);
        tutorialToSave.setCreationTutorialDate(LocalDateTime.now());
        tutorialToSave.setExternalId(IdGenerator.generateExternalId());
        tutorialToSave.setWriter(writer);

        return tutorialMapper.fromEntityToDto(tutorialRepository.save(tutorialToSave));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public TutorialDto updateTutorial(TutorialDto tutorialDto) {
        mapTutorialStatus(tutorialDto);
        Tutorial tutorialToSave = tutorialMapper.fromDtoToEntity(tutorialDto);

        Tutorial oldVersion =
                tutorialRepository.findByExternalId(tutorialToSave.getExternalId()).orElse(null);
        if (oldVersion == null)
            throw new FunctionalException("Error in tutorial update processus, old version not found in DB");

        tutorialToSave.setUpdateTutorialDate(LocalDateTime.now());
        tutorialToSave.setTutorialId(oldVersion.getTutorialId());
        tutorialToSave.setWriter(oldVersion.getWriter());
        tutorialToSave.setChapters(oldVersion.getChapters());
        tutorialToSave.setRemoteRepository(oldVersion.getRemoteRepository());

        return tutorialMapper.fromEntityToDto(tutorialRepository.save(tutorialToSave));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public TutorialDto getTutorial(String externalId) {
        try{
            return tutorialMapper.fromEntityToDto(
                    tutorialRepository.findByExternalId(externalId).orElse(null)
            );
        }catch (HibernateException e){
            log.error("Error during search in db for tutorial by externalID : " + e.getMessage());
            throw new FunctionalException("Error during search in db for tutorial by externalID : " + e.getMessage());
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Page<TutorialDto> getTutorials(Status status, String keyword, int page) {
        log.debug("*** search : " + keyword);
        try{
            Pageable requestedPage = PageRequest.of((page), 6, Sort.by(Sort.Direction.ASC, "publicationDate"));
            Page<Tutorial> tutorials;
            if (keyword.equalsIgnoreCase("all")) {
                log.debug("*** no keyword");
                tutorials = tutorialRepository.findAllByStatus(status, requestedPage);
            }else {
                log.debug("*** keyword : " + keyword);
                tutorials = tutorialRepository.findAllByKeywordAndStatus(keyword.toUpperCase(), status, requestedPage);
            }
            return tutorials.map(tutorialMapper::fromEntityToDto);
        }catch (HibernateException e){
            String errorMessage = "Error during search in db for all tutorials with status " +
                    (status != null ? status.getValue() : "all") +
                    (keyword != null && !keyword.equalsIgnoreCase("") ? " and keyword " + keyword : "") +
                    " : " + e.getMessage();
            log.error(errorMessage);
            throw new FunctionalException(errorMessage);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<TutorialDto> getTutorialsByWriterExternalId(String externalId){
        try{
            return tutorialMapper.fromEntityListToDtoList(
                    tutorialRepository.findAllByWriterExternalId(externalId, Sort.by(Sort.Direction.ASC, "creationTutorialDate"))
            );
        }catch (HibernateException e){
            log.error("Error during search in db for tutorial by writer externalID : " + e.getMessage());
            throw new FunctionalException("Error during search in db for tutorial by writer externalID : " + e.getMessage());
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public RepositoryDto addRepositoryToTutorial(String tutorialExternalId, RepositoryDto repositoryDto) {

        try{
            Tutorial tutorial = tutorialRepository.findByExternalId(tutorialExternalId).orElse(null);
            if (tutorial == null) throw new FunctionalException("Error during insertion repository in tutorial, tutorial not found!");

            SecurityUtils.checkIfAuthorized(tutorial.getWriter().getExternalId());

            repositoryDto.setExternalId(IdGenerator.generateExternalId());
            repositoryDto.setAddRepositoryDate(LocalDateTime.now());
            tutorial.setRemoteRepository(remoteRepository.save(repositoryMapper.fromDtoToEntity(repositoryDto)));

            return repositoryDto;
        }catch (HibernateException e){
            log.error("error during repository insertion in tutorial with extId : " + tutorialExternalId);
            throw new FunctionalException("error during repository insertion : " + e.getMessage());
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void removeRepositoryFromTutorial(String repositoryExternalId) {
        try{
            Tutorial tutorial = tutorialRepository.findTutorialWithRepositoryToRemove(repositoryExternalId);

            SecurityUtils.checkIfAuthorized(tutorial.getWriter().getExternalId());

            tutorial.setRemoteRepository(null);
        }catch (HibernateException e){
            log.error("error during repository deletion from tutorial");
            throw new FunctionalException("error during repository deletion : " + e.getMessage());
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public TutorialDto updateTutorialStatus(String externalId, Status status) {
        Tutorial oldVersion = tutorialRepository.findByExternalId(externalId).orElse(null);
        if (oldVersion == null)
            throw new FunctionalException("Error in tutorial update processus, old version not found in DB");

        SecurityUtils.checkIfAuthorized(oldVersion.getWriter().getExternalId());

        oldVersion.setStatus(status);
        oldVersion.setPublicationDate(LocalDateTime.now());
        return tutorialMapper.fromEntityToDto(oldVersion);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void deleteTutorial(String externalId){
        try{
            Tutorial tutorialToDelete = tutorialRepository.findByExternalId(externalId).orElse(null);
            if (tutorialToDelete == null) throw new FunctionalException("Tutorial to delete not found in DB.");

            SecurityUtils.checkIfAuthorized(tutorialToDelete.getWriter().getExternalId());

            tutorialRepository.delete(tutorialToDelete);
        }catch (HibernateException e){
            log.error("error during tutorial deletion with extId : " + externalId);
            throw new FunctionalException("error during tutorial deletion : " + e.getMessage());
        }
    }

    /**
     * This method apply layout to title ( first letter uppercase and the other lowercase)
     * @param tutorialDto tutorial it work on
     */
    private void setTitleLayout(TutorialDto tutorialDto) {
        if (tutorialDto.getTitle() == null) throw new FunctionalException("Error ! Tutorial to save has no title");
        String firstCaracter = tutorialDto.getTitle().substring(0,1);
        String title = tutorialDto.getTitle().toLowerCase();
        tutorialDto.setTitle(
                title.replaceFirst(firstCaracter,firstCaracter.toUpperCase())
        );
    }

    /**
     * This method map enum attribut value to string value of enum type
     * @param tutorialDto tutorial to work with
     */
    private void mapTutorialStatus(TutorialDto tutorialDto) {
        if (tutorialDto.getStatus() == null) throw new FunctionalException("Error ! Tutorial to update has no status");

        switch(tutorialDto.getStatus()) {
            case "Non publié" :
                tutorialDto.setStatus(String.valueOf(Status.NOT_PUPLISHED));
                break;
            case "Publié" :
                tutorialDto.setStatus(String.valueOf(Status.PUBLISHED));
                break;
            case "En attente" :
                tutorialDto.setStatus(String.valueOf(Status.NEED_VALIDATION));
                break;
            default :
                throw new FunctionalException("No enum value corresponding for : " + tutorialDto.getStatus());
        }
    }

}
