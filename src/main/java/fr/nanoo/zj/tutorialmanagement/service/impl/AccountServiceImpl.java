package fr.nanoo.zj.tutorialmanagement.service.impl;

import fr.nanoo.zj.tutorialmanagement.db.AccountRepository;
import fr.nanoo.zj.tutorialmanagement.model.dtos.AccountDto;
import fr.nanoo.zj.tutorialmanagement.model.entities.Account;
import fr.nanoo.zj.tutorialmanagement.model.mappers.AccountMapper;
import fr.nanoo.zj.tutorialmanagement.service.contract.AccountService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

/**
 * @author nanoo - created : 10/06/2020 - 19:41
 */
@Service
@Slf4j
@Transactional
public class AccountServiceImpl implements AccountService {

    private final AccountRepository accountRepository;
    private final AccountMapper accountMapper;

    @Autowired
    public AccountServiceImpl(AccountRepository accountRepository, AccountMapper accountMapper) {
        this.accountRepository = accountRepository;
        this.accountMapper = accountMapper;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public AccountDto saveUser(AccountDto user) {
        return accountMapper.fromEntityToDto(
                accountRepository.save(accountMapper.fromDtoToEntity(user))
        );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void deleteUserAccount(String id) {
        accountRepository.deleteByExternalId(id);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Account getUserAccount(String externalId) {
        return accountRepository.findByExternalId(externalId).orElse(null);
    }

    /**
     * This method return user external Id if he's logged in api.
     * @return external Id or "" if no user logged
     */
    public static String getUserLoggedExternalId(){
        return SecurityContextHolder.getContext().getAuthentication() != null ?
                SecurityContextHolder.getContext().getAuthentication().getCredentials().toString() :
                null;
    }
}
