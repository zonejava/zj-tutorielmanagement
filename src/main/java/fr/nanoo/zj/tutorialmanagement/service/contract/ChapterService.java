package fr.nanoo.zj.tutorialmanagement.service.contract;

import fr.nanoo.zj.tutorialmanagement.model.dtos.ChapterDto;
import fr.nanoo.zj.tutorialmanagement.model.dtos.VideoDto;

/**
 * @author nanoo - created : 12/06/2020 - 19:06
 */
public interface ChapterService {

    /**
     * This method add a chapter to a tutorial and save it in DB.
     * @param chapterDto chapter to add
     * @param tutorialExternalId tutorial who receive the chapter
     * @return chapter if success
     */
    ChapterDto addChapterToTutorial(ChapterDto chapterDto, String tutorialExternalId);

    /**
     * This method made change in chapter and save it in DB.
     * @param chapterDto chapter mofidied
     * @return chapter if success
     */
    ChapterDto updateChapter(ChapterDto chapterDto);

    /**
     * This methode remove a chapter from DB.
     * @param chapterExternalId chapter external id to remove
     */
    void removeChapterFromTutorial(String chapterExternalId);

    /**
     * This method add a video to a chapter and save it in DB
     * @param videoDto video to add
     * @param chapterExternalId chapter we add a video
     */
    void addVideoToChapter(VideoDto videoDto, String chapterExternalId);

    /**
     * This video remove a video from a chapter and DB
     * @param videoExterenalId video to remove
     */
    void removeVideoFromChapter(String videoExterenalId);

}
