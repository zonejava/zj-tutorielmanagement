package fr.nanoo.zj.tutorialmanagement.model.dtos;

import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @author nanoo - created : 10/06/2020 - 14:56
 */
@Data
public class VideoDto implements Serializable {

    private static final long serialVersionUID = 1L;

    private String externalId;
    private String source;
    private String link;
    private LocalDateTime addVideoDate;

}
