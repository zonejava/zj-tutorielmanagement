package fr.nanoo.zj.tutorialmanagement.model.enumerations;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author nanoo - created : 10/06/2020 - 12:46
 */
@AllArgsConstructor
public enum Accessibility {

    BEGINNER("Débutant"),
    INTERMEDIATE("Intermédiaire"),
    CONFIRMED("Confirmé");

    @Getter private final String value;

}
