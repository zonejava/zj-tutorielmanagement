package fr.nanoo.zj.tutorialmanagement.model.dtos;

import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @author nanoo - created : 10/06/2020 - 14:53
 */
@Data
public class ChapterDto implements Serializable {

    private static final long serialVersionUID = 1L;

    private String externalId;
    private String title;
    private String content;
    private VideoDto video;
    private LocalDateTime creationChapterDate;
    private LocalDateTime updateChapterDate;

}
