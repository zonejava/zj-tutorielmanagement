package fr.nanoo.zj.tutorialmanagement.model.dtos;

import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @author nanoo - created : 10/06/2020 - 12:51
 */
@Data
public class RateDto implements Serializable {

    private static final long serialVersionUID = 1L;

    private String externalId;
    private AccountDto critic;
    private int comprehensibilityScore;
    private int redactionScore;
    private int relevanceScore;
    private LocalDateTime creationRateDate;
    private LocalDateTime updateRateDate;

}
