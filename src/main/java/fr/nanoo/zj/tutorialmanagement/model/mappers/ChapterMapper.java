package fr.nanoo.zj.tutorialmanagement.model.mappers;

import fr.nanoo.zj.tutorialmanagement.model.dtos.ChapterDto;
import fr.nanoo.zj.tutorialmanagement.model.entities.Chapter;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

import java.util.List;

/**
 * @author nanoo - created : 10/06/2020 - 17:48
 */
@Mapper(componentModel = "spring",
        unmappedTargetPolicy = ReportingPolicy.IGNORE,
        uses = {VideoMapper.class})
public interface ChapterMapper {

    Chapter fromDtoToEntity (ChapterDto chapterDto);
    ChapterDto fromEntityToDto (Chapter chapter);
    List<Chapter> fromDtoListToEntityList (List<ChapterDto> tutorialDtos);
    List<ChapterDto> fromEntityListToDtoList (List<Chapter> chapters);

}
