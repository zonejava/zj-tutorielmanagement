package fr.nanoo.zj.tutorialmanagement.model.entities;

import fr.nanoo.zj.tutorialmanagement.model.enumerations.Accessibility;
import fr.nanoo.zj.tutorialmanagement.model.enumerations.Status;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @author nanoo - created : 10/06/2020 - 12:37
 */
@Data
@Entity
@Table(name = "tutorial")
public class Tutorial implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id_tutorial")
    private Long tutorialId;

    @Column(name = "external_id",unique = true, nullable = false)
    private String externalId;

    @Column(name = "title", length = 100,nullable = false)
    private String title;

    @Column(name = "description", length = 500, nullable = false)
    private String description;

    @ManyToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name="id_account", nullable=false)
    private Account writer;

    @ManyToMany
    @JoinTable(
            name = "tutorial_themes",
            joinColumns = @JoinColumn(name = "id_tutorial"),
            inverseJoinColumns = @JoinColumn(name = "id_tag"))
    private List<Tag> themes;

    @OneToMany(mappedBy="tutorial", fetch = FetchType.LAZY, orphanRemoval = true, cascade = CascadeType.ALL)
    private List<Chapter> chapters;

    @OneToMany(mappedBy="tutorial", fetch = FetchType.LAZY, orphanRemoval = true, cascade = CascadeType.ALL)
    private List<Rate> rate;

    @OneToOne(orphanRemoval = true, cascade = CascadeType.ALL)
    @JoinColumn(name = "id_repository", referencedColumnName = "id_repository")
    private RemoteRepository remoteRepository;

    @Enumerated(EnumType.STRING)
    @Column(name = "accessibility")
    private Accessibility accessibility;

    @Enumerated(EnumType.STRING)
    @Column(name = "status", nullable = false)
    private Status status;

    @Column(name = "publication_tutorial")
    private LocalDateTime publicationDate;

    @Column(name = "creation_tutorial", nullable = false)
    private LocalDateTime creationTutorialDate;

    @Column(name = "update_tutorial")
    private LocalDateTime updateTutorialDate;

}
