package fr.nanoo.zj.tutorialmanagement.model.entities;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

/**
 * @author nanoo - created : 10/06/2020 - 14:51
 */
@Data
@Entity
@Table(name = "tag")
public class Tag implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id_tag")
    private Long tagId;

    @Column(name = "external_id",unique = true, nullable = false)
    private String externalId;

    @Column(name = "name",unique = true, nullable = false)
    private String name;

    @ManyToMany(mappedBy = "themes")
    private List<Tutorial> tutorials;

}
