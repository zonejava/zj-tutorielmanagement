package fr.nanoo.zj.tutorialmanagement.model.entities;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @author nanoo - created : 10/06/2020 - 12:51
 */
@Data
@Entity
@Table(name = "rate")
public class Rate implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id_rate")
    private Long rateId;

    @Column(name = "external_id",unique = true, nullable = false)
    private String externalId;

    @ManyToOne
    @JoinColumn(name="id_account", nullable=false)
    private Account critic;

    @Column(name = "comprehensibility_score",unique = true, nullable = false)
    private int comprehensibilityScore;

    @Column(name = "redaction_score",unique = true, nullable = false)
    private int redactionScore;

    @Column(name = "relevance_score",unique = true, nullable = false)
    private int relevanceScore;

    @ManyToOne
    @JoinColumn(name="id_tutorial", nullable=false)
    private Tutorial tutorial;

    @Column(name = "creation_rate", nullable = false)
    private LocalDateTime creationRateDate;

    @Column(name = "update_rate")
    private LocalDateTime updateRateDate;
}
