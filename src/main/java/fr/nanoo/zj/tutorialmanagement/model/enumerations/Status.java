package fr.nanoo.zj.tutorialmanagement.model.enumerations;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author nanoo - created : 10/06/2020 - 16:13
 */
@AllArgsConstructor
public enum Status {

    NOT_PUPLISHED("Non publié"),
    PUBLISHED("Publié"),
    NEED_VALIDATION("En attente");

    @Getter private final String value;

}
