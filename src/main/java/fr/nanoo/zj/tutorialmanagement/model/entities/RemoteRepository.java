package fr.nanoo.zj.tutorialmanagement.model.entities;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @author nanoo - created : 10/06/2020 - 15:25
 */
@Data
@Entity
@Table(name = "repository")
public class RemoteRepository implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id_repository")
    private Long repositoryId;

    @Column(name = "external_id",unique = true, nullable = false)
    private String externalId;

    @Column(name = "source", length = 50, nullable = false)
    private String source;

    @Column(name = "link", length = 500, nullable = false)
    private String link;

    @Column(name = "upload_repository", nullable = false)
    private LocalDateTime addRepositoryDate;

}
