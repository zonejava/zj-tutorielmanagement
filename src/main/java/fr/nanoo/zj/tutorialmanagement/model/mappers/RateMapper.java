package fr.nanoo.zj.tutorialmanagement.model.mappers;

import fr.nanoo.zj.tutorialmanagement.model.dtos.RateDto;
import fr.nanoo.zj.tutorialmanagement.model.entities.Rate;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

/**
 * @author nanoo - created : 10/06/2020 - 17:55
 */
@Mapper(componentModel = "spring",
        unmappedTargetPolicy = ReportingPolicy.IGNORE,
        uses = {AccountMapper.class})
public interface RateMapper {

    Rate fromDtoToEntity (RateDto rateDto);
    RateDto fromEntityToDto (Rate rate);

}
