package fr.nanoo.zj.tutorialmanagement.model.mappers;

import fr.nanoo.zj.tutorialmanagement.model.dtos.AccountDto;
import fr.nanoo.zj.tutorialmanagement.model.entities.Account;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

/**
 * @author nanoo - created : 10/06/2020 - 17:53
 */
@Mapper(componentModel = "spring",
        unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface AccountMapper {

    Account fromDtoToEntity (AccountDto accountDto);
    AccountDto fromEntityToDto (Account account);

}
