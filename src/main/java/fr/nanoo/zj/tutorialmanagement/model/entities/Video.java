package fr.nanoo.zj.tutorialmanagement.model.entities;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @author nanoo - created : 10/06/2020 - 14:56
 */
@Data
@Entity
@Table(name = "video")
public class Video implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id_video")
    private Long videoId;

    @Column(name = "external_id",unique = true, nullable = false)
    private String externalId;

    @Column(name = "source", length = 50, nullable = false)
    private String source;

    @Column(name = "link", length = 500, nullable = false)
    private String link;

    @Column(name = "upload_video", nullable = false)
    private LocalDateTime addVideoDate;

}
