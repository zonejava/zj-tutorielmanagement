package fr.nanoo.zj.tutorialmanagement.model.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @author nanoo - created : 10/06/2020 - 14:53
 */
@Data
@Entity
@Table(name = "chapter")
public class Chapter implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id_chapter")
    private Long chapterId;

    @Column(name = "external_id",unique = true, nullable = false)
    private String externalId;

    @Column(name = "title", length = 100, nullable = false)
    private String title;

    @Lob
    @Column(name = "content", nullable = false)
    private String content;

    @OneToOne(orphanRemoval = true, cascade = CascadeType.ALL)
    @JoinColumn(name = "id_video", referencedColumnName = "id_video")
    private Video video;

    @JsonIgnore
    @ManyToOne()
    @JoinColumn(name="id_tutorial", nullable=false)
    private Tutorial tutorial;

    @Column(name = "creation_chapter", nullable = false)
    private LocalDateTime creationChapterDate;

    @Column(name = "update_chapter")
    private LocalDateTime updateChapterDate;

}
