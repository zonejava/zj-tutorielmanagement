package fr.nanoo.zj.tutorialmanagement.model.mappers;

import fr.nanoo.zj.tutorialmanagement.model.dtos.VideoDto;
import fr.nanoo.zj.tutorialmanagement.model.entities.Video;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

/**
 * @author nanoo - created : 10/06/2020 - 17:59
 */
@Mapper(componentModel = "spring",
        unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface VideoMapper {

    Video fromDtoToEntity (VideoDto videoDto);
    VideoDto fromEntityToDto (Video video);

}
