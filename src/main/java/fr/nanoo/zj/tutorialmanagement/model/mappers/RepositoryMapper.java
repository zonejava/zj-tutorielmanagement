package fr.nanoo.zj.tutorialmanagement.model.mappers;

import fr.nanoo.zj.tutorialmanagement.model.dtos.RepositoryDto;
import fr.nanoo.zj.tutorialmanagement.model.entities.RemoteRepository;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

/**
 * @author nanoo - created : 10/06/2020 - 17:57
 */
@Mapper(componentModel = "spring",
        unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface RepositoryMapper {

    RemoteRepository fromDtoToEntity (RepositoryDto repositoryDto);
    RepositoryDto fromEntityToDto (RemoteRepository remoteRepository);

}
