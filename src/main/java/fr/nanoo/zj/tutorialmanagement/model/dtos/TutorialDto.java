package fr.nanoo.zj.tutorialmanagement.model.dtos;

import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @author nanoo - created : 10/06/2020 - 12:37
 */
@Data
public class TutorialDto implements Serializable {

    private static final long serialVersionUID = 1L;

    private String externalId;
    private String title;
    private String description;
    private AccountDto writer;
    private List<TagDto> themes;
    private List<ChapterDto> chapters;
    private List<RateDto> rate;
    private RepositoryDto remoteRepository;
    private String accessibility;
    private String status;
    private LocalDateTime publicationDate;
    private LocalDateTime creationTutorialDate;
    private LocalDateTime updateTutorialDate;

}
