package fr.nanoo.zj.tutorialmanagement.model.mappers;

import fr.nanoo.zj.tutorialmanagement.model.dtos.TagDto;
import fr.nanoo.zj.tutorialmanagement.model.entities.Tag;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

/**
 * @author nanoo - created : 10/06/2020 - 17:58
 */
@Mapper(componentModel = "spring",
        unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface TagMapper {

    Tag fromDtoToEntity (TagDto tagDto);
    TagDto fromEntityToDto (Tag tag);

}
