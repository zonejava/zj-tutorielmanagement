package fr.nanoo.zj.tutorialmanagement.model.mappers;

import fr.nanoo.zj.tutorialmanagement.model.dtos.TutorialDto;
import fr.nanoo.zj.tutorialmanagement.model.entities.Tutorial;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;

import java.util.List;

/**
 * @author nanoo - created : 10/06/2020 - 17:30
 */
@Mapper(componentModel = "spring",
        unmappedTargetPolicy = ReportingPolicy.IGNORE,
        uses = {AccountMapper.class, TagMapper.class, ChapterMapper.class, RateMapper.class, RepositoryMapper.class})
public interface TutorialMapper {

    @Mapping(source = "status.value", target = "status")
    @Mapping(source = "accessibility.value", target = "accessibility")
    TutorialDto fromEntityToDto(Tutorial tutorial);
    Tutorial fromDtoToEntity(TutorialDto tutorialDto);

    List<Tutorial> fromDtoListToEntityList(List<TutorialDto> tutorialDtos);
    List<TutorialDto> fromEntityListToDtoList(List<Tutorial> tutorials);

}
