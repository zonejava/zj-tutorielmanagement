package fr.nanoo.zj.tutorialmanagement.model.dtos;

import lombok.Data;

import java.io.Serializable;

/**
 * @author nanoo - created : 10/06/2020 - 14:51
 */
@Data
public class TagDto implements Serializable {

    private static final long serialVersionUID = 1L;

    private String externalId;
    private String name;

}
