package fr.nanoo.zj.tutorialmanagement.db;

import fr.nanoo.zj.tutorialmanagement.model.entities.Rate;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author nanoo - created : 10/06/2020 - 18:18
 */
@Repository
public interface RateRepository extends JpaRepository<Rate,Long> {
}
