package fr.nanoo.zj.tutorialmanagement.db;

import fr.nanoo.zj.tutorialmanagement.model.entities.Account;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * @author nanoo - created : 10/06/2020 - 18:16
 */
@Repository
public interface AccountRepository extends JpaRepository<Account,Long> {

    @Modifying
    @Query(value="DELETE FROM Account user WHERE user.externalId = :externalId")
    void deleteByExternalId(String externalId);

    Optional<Account> findByExternalId(String externalId);

}
