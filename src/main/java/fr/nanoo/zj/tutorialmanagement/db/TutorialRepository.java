package fr.nanoo.zj.tutorialmanagement.db;

import fr.nanoo.zj.tutorialmanagement.model.entities.Tutorial;
import fr.nanoo.zj.tutorialmanagement.model.enumerations.Status;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * @author nanoo - created : 10/06/2020 - 18:13
 */
@Repository
public interface TutorialRepository extends JpaRepository<Tutorial, Long> {

    Optional<Tutorial> findByExternalId(String externalId);

    Page<Tutorial> findAllByStatus(Status status, Pageable pageable);

    @Query(value = "SELECT t FROM Tutorial t WHERE t.writer.externalId = :externalId")
    List<Tutorial> findAllByWriterExternalId(@Param("externalId") String externalId, Sort sort);

    @Query(value = "SELECT t FROM Tutorial t WHERE t.remoteRepository.externalId = :repositoryExternalId")
    Tutorial findTutorialWithRepositoryToRemove(@Param("repositoryExternalId") String repositoryExternalId);

    @Query(value = "SELECT t FROM Tutorial t WHERE upper(CONCAT(t.title, ' ', t.description)) LIKE %:keyword% AND t.status = :status")
    Page<Tutorial> findAllByKeywordAndStatus(@Param("keyword") String keyword, @Param("status") Status status, Pageable requestedPage);

}
