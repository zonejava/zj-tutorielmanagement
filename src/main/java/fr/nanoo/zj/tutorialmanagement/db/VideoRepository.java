package fr.nanoo.zj.tutorialmanagement.db;

import fr.nanoo.zj.tutorialmanagement.model.entities.Video;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author nanoo - created : 10/06/2020 - 18:23
 */
@Repository
public interface VideoRepository extends JpaRepository<Video,Long> {

}
