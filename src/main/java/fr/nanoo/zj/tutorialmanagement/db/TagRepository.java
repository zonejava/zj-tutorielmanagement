package fr.nanoo.zj.tutorialmanagement.db;

import fr.nanoo.zj.tutorialmanagement.model.entities.Tag;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author nanoo - created : 10/06/2020 - 18:22
 */
@Repository
public interface TagRepository extends JpaRepository<Tag,Long> {
}
