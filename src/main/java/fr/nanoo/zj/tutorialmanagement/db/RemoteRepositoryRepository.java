package fr.nanoo.zj.tutorialmanagement.db;

import fr.nanoo.zj.tutorialmanagement.model.entities.RemoteRepository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author nanoo - created : 10/06/2020 - 18:19
 */
@Repository
public interface RemoteRepositoryRepository extends JpaRepository<RemoteRepository,Long> {
}
