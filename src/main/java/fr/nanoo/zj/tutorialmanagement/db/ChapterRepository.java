package fr.nanoo.zj.tutorialmanagement.db;

import fr.nanoo.zj.tutorialmanagement.model.entities.Chapter;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * @author nanoo - created : 10/06/2020 - 18:15
 */
@Repository
public interface ChapterRepository extends JpaRepository<Chapter,Long> {

    Optional<Chapter> findByExternalId(String externalId);

    @Query(value = "SELECT c FROM Chapter c WHERE c.video.externalId = :videoExterenalId")
    Chapter findChapterWithVideoToRemove(@Param("videoExterenalId") String videoExterenalId);
}
