package fr.nanoo.zj.tutorialmanagement.service.impl;

import fr.nanoo.zj.tutorialmanagement.application.errors.FunctionalException;
import fr.nanoo.zj.tutorialmanagement.db.ChapterRepository;
import fr.nanoo.zj.tutorialmanagement.db.TutorialRepository;
import fr.nanoo.zj.tutorialmanagement.db.VideoRepository;
import fr.nanoo.zj.tutorialmanagement.model.dtos.ChapterDto;
import fr.nanoo.zj.tutorialmanagement.model.dtos.VideoDto;
import fr.nanoo.zj.tutorialmanagement.model.mappers.*;
import fr.nanoo.zj.tutorialmanagement.service.contract.ChapterService;
import org.hibernate.HibernateException;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@SpringJUnitConfig(value = {
        ChapterServiceImpl.class, ChapterMapperImpl.class, VideoMapperImpl.class
})
class ChapterServiceImplTest {

    @Autowired
    private ChapterService classUnderTest;
    @Autowired
    private ChapterMapper chapterMapper;

    @MockBean
    private TutorialRepository tutorialRepository;
    @MockBean
    private ChapterRepository chapterRepository;
    @MockBean
    private VideoRepository videoRepository;

    @Test
    void whenUpdateChapter_GivenNoChapterInDb_ThenThrowFunctionalException() {
        when(chapterRepository.findByExternalId(any(String.class))).thenReturn(Optional.empty());

        FunctionalException exception =
                assertThrows(FunctionalException.class, () -> classUnderTest.updateChapter(new ChapterDto()));

        assertThat(exception.getMessage()).isEqualTo("Error in chapter update processus, old version not found in DB");
    }

    @Test
    void whenRemoveChapterFromTutorial_GivenNoChapterInDb_ThenThrowFunctionalException() {
        when(chapterRepository.findByExternalId(any(String.class))).thenReturn(Optional.empty());

        FunctionalException exception =
                assertThrows(FunctionalException.class, () -> classUnderTest.removeChapterFromTutorial("test"));

        assertThat(exception.getMessage()).isEqualTo("Error, chapter not found in DB");
    }

    @Test
    void whenRemoveChapterFromTutorial_GivenErrorThrownInChapterRpository_ThenThrowFunctionalException() {
        when(chapterRepository.findByExternalId(any(String.class))).thenThrow(new HibernateException("error test"));

        FunctionalException exception =
                assertThrows(FunctionalException.class, () -> classUnderTest.removeChapterFromTutorial("test"));

        assertThat(exception.getMessage()).isEqualTo("error during chapter deletion : error test");
    }

    @Test
    void whenAddVideoToChapter_GivenNoChapterInDb_ThenThrowFunctionalException() {
        when(chapterRepository.findByExternalId(any(String.class))).thenReturn(Optional.empty());

        FunctionalException exception =
                assertThrows(FunctionalException.class, () -> classUnderTest.addVideoToChapter(new VideoDto(),"test"));

        assertThat(exception.getMessage()).isEqualTo("Error during insertion video in chapter, chapter not found!");
    }

    @Test
    void whenAddVideoToChapter_GivenErrorThrownInChapterRpository_ThenThrowFunctionalException() {
        when(chapterRepository.findByExternalId(any(String.class))).thenThrow(new HibernateException("error test"));

        FunctionalException exception =
                assertThrows(FunctionalException.class, () -> classUnderTest.addVideoToChapter(new VideoDto(),"test"));

        assertThat(exception.getMessage()).isEqualTo("error during video insertion : error test");
    }

    @Test
    void whenRemoveVideoToChapter_GivenErrorThrownInChapterRpository_ThenThrowFunctionalException() {
        when(chapterRepository.findChapterWithVideoToRemove(any(String.class))).thenThrow(new HibernateException("error test"));

        FunctionalException exception =
                assertThrows(FunctionalException.class, () -> classUnderTest.removeVideoFromChapter("test"));

        assertThat(exception.getMessage()).isEqualTo("error during video deletion : error test");
    }
}