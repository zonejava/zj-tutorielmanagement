package fr.nanoo.zj.tutorialmanagement.service.impl;

import fr.nanoo.zj.tutorialmanagement.application.errors.FunctionalException;
import fr.nanoo.zj.tutorialmanagement.db.RemoteRepositoryRepository;
import fr.nanoo.zj.tutorialmanagement.db.TutorialRepository;
import fr.nanoo.zj.tutorialmanagement.model.dtos.RepositoryDto;
import fr.nanoo.zj.tutorialmanagement.model.dtos.TutorialDto;
import fr.nanoo.zj.tutorialmanagement.model.enumerations.Status;
import fr.nanoo.zj.tutorialmanagement.model.mappers.*;
import fr.nanoo.zj.tutorialmanagement.service.contract.AccountService;
import fr.nanoo.zj.tutorialmanagement.service.contract.TutorialService;
import org.hibernate.HibernateException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.when;

@SpringJUnitConfig(value = {
        TutorialServiceImpl.class, TutorialMapperImpl.class, RepositoryMapperImpl.class, AccountMapperImpl.class,
        TagMapperImpl.class, ChapterMapperImpl.class, VideoMapperImpl.class, RateMapperImpl.class
})
class TutorialServiceImplTest {

    @Autowired
    private TutorialService classUnderTest;
    @Autowired
    private TutorialMapper tutorialMapper;
    @Autowired
    private RepositoryMapper repositoryMapper;

    @MockBean
    private TutorialRepository tutorialRepository;
    @MockBean
    private RemoteRepositoryRepository remoteRepository;
    @MockBean
    private AccountService accountService;

    private TutorialDto tutorialDto;
    private RepositoryDto repositoryDto;

    @BeforeEach
    public void init(){
        tutorialDto = new TutorialDto();
        repositoryDto = new RepositoryDto();
    }

    @Test
    void whenSaveTutorial_GivenTutorialWithNoTitle_ThenThrowFunctionalException() {
        FunctionalException exception =
                assertThrows(FunctionalException.class, () -> classUnderTest.saveTutorial(tutorialDto));

        assertThat(exception.getMessage()).isEqualTo("Error ! Tutorial to save has no title");
    }

    @Test
    void whenSaveTutorial_GivenTutorialButNoUserLogged_ThenThrowFunctionalException() {
        when(accountService.getUserAccount(any(String.class))).thenReturn(null);
        tutorialDto.setTitle("test");

        FunctionalException exception =
                assertThrows(FunctionalException.class, () -> classUnderTest.saveTutorial(tutorialDto));

        assertThat(exception.getMessage()).isEqualTo("No writer account found in DB");
    }

    @Test
    void whenUpdateTutorial_GivenTutorialButWithoutStatus_ThenThrowFunctionalException() {
        FunctionalException exception =
                assertThrows(FunctionalException.class, () -> classUnderTest.updateTutorial(tutorialDto));

        assertThat(exception.getMessage()).isEqualTo("Error ! Tutorial to update has no status");
    }

    @Test
    void whenUpdateTutorial_GivenTutorialButWithWringStatus_ThenThrowFunctionalException() {
        tutorialDto.setStatus("test");

        FunctionalException exception =
                assertThrows(FunctionalException.class, () -> classUnderTest.updateTutorial(tutorialDto));

        assertThat(exception.getMessage()).isEqualTo("No enum value corresponding for : test");
    }

    @Test
    void whenUpdateTutorial_GivenNoTutorialInDb_ThenThrowFunctionalException() {
        when(tutorialRepository.findByExternalId(any(String.class))).thenReturn(null);
        tutorialDto.setStatus("Publié");

        FunctionalException exception =
                assertThrows(FunctionalException.class, () -> classUnderTest.updateTutorial(tutorialDto));

        assertThat(exception.getMessage()).isEqualTo("Error in tutorial update processus, old version not found in DB");
    }

    @Test
    void whenGetTutorial_GivenErrorThrownInTutorialRepository_ThenThrowFunctionalException() {
        when(tutorialRepository.findByExternalId(any(String.class))).thenThrow(new HibernateException("not found"));

        FunctionalException exception =
                assertThrows(FunctionalException.class, () -> classUnderTest.getTutorial("test"));

        assertThat(exception.getMessage()).isEqualTo("Error during search in db for tutorial by externalID : not found");
    }

    @Test
    void whenGetTutorials_GivenErrorThrownInTutorialRepository_ThenThrowFunctionalException() {
        when(tutorialRepository.findAllByStatus(any(Status.class), any(PageRequest.class))).thenThrow(new HibernateException("not found"));

        FunctionalException exception =
                assertThrows(FunctionalException.class, () -> classUnderTest.getTutorials(Status.PUBLISHED,"all", 0));

        assertThat(exception.getMessage()).isEqualTo("Error during search in db for all tutorials with status Publié and keyword all : not found");
    }

    @Test
    void whenGetTutorialsByWriterExternalId_GivenErrorThrownInTutorialRepository_ThenThrowFunctionalException() {
        when(tutorialRepository.findAllByWriterExternalId(any(String.class), any(Sort.class))).thenThrow(new HibernateException("not found"));

        FunctionalException exception =
                assertThrows(FunctionalException.class, () -> classUnderTest.getTutorialsByWriterExternalId("test"));

        assertThat(exception.getMessage()).isEqualTo("Error during search in db for tutorial by writer externalID : not found");
    }

    @Test
    void whenAddRepositoryToTutorial_GivenNoTutorialInDb_ThenThrowFunctionalException() {
        when(tutorialRepository.findByExternalId(any(String.class))).thenReturn(Optional.empty());

        FunctionalException exception =
                assertThrows(FunctionalException.class, () -> classUnderTest.addRepositoryToTutorial("test",repositoryDto));

        assertThat(exception.getMessage()).isEqualTo("Error during insertion repository in tutorial, tutorial not found!");
    }

    @Test
    void whenAddRepositoryToTutorial_GivenErrorThrownInTutorialRespository_ThenThrowFunctionalException() {
        when(tutorialRepository.findByExternalId(any(String.class))).thenThrow(new HibernateException("not found"));

        FunctionalException exception =
                assertThrows(FunctionalException.class, () -> classUnderTest.addRepositoryToTutorial("test",repositoryDto));

        assertThat(exception.getMessage()).isEqualTo("error during repository insertion : not found");
    }

    @Test
    void whenRemoveRepositoryToTutorial_GivenErrorThrownInTutorialRespository_ThenThrowFunctionalException() {
        when(tutorialRepository.findTutorialWithRepositoryToRemove(any(String.class))).thenThrow(new HibernateException("not found"));

        FunctionalException exception =
                assertThrows(FunctionalException.class, () -> classUnderTest.removeRepositoryFromTutorial("test"));

        assertThat(exception.getMessage()).isEqualTo("error during repository deletion : not found");
    }

    @Test
    void whenUpdateTutorialStatus_GivenNoTutorialInDb_ThenThrowFunctionalException() {
        when(tutorialRepository.findByExternalId(any(String.class))).thenReturn(Optional.empty());

        FunctionalException exception =
                assertThrows(FunctionalException.class, () -> classUnderTest.updateTutorialStatus("test",Status.PUBLISHED));

        assertThat(exception.getMessage()).isEqualTo("Error in tutorial update processus, old version not found in DB");
    }

    @Test
    void whenDeleteTutorial_GivenNoTutorialInDb_ThenThrowFunctionalException() {
        when(tutorialRepository.findByExternalId(any(String.class))).thenReturn(Optional.empty());

        FunctionalException exception =
                assertThrows(FunctionalException.class, () -> classUnderTest.deleteTutorial("test"));

        assertThat(exception.getMessage()).isEqualTo("Tutorial to delete not found in DB.");
    }

    @Test
    void whenDeleteTutorial_GivenErrorThrownInTutorialRespository_ThenThrowFunctionalException() {
        when(tutorialRepository.findByExternalId(any(String.class))).thenThrow(new HibernateException("error test"));

        FunctionalException exception =
                assertThrows(FunctionalException.class, () -> classUnderTest.deleteTutorial("test"));

        assertThat(exception.getMessage()).isEqualTo("error during tutorial deletion : error test");
    }
}